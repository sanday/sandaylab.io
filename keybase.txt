==================================================================
https://keybase.io/sanday_
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://sanday.gitlab.io
  * I am sanday_ (https://keybase.io/sanday_) on keybase.
  * I have a public key with fingerprint 4B1B 1892 89D3 9E7A E0F5  5AD5 D70B 96F7 DA9B D4ED

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "012062b64d0281e8226c02f416ec94ab2210c3069d59adb320cce9da5b6afa107a3e0a",
      "fingerprint": "4b1b189289d39e7ae0f55ad5d70b96f7da9bd4ed",
      "host": "keybase.io",
      "key_id": "d70b96f7da9bd4ed",
      "kid": "0101c60fdfb90b2be035318616667435741174723572617ca33fc77f5eacb3a656970a",
      "uid": "22b9262b7f28c316b121a1f310cfe919",
      "username": "sanday_"
    },
    "merkle_root": {
      "ctime": 1532390394,
      "hash_meta": "fce0e5c1eef26d764e5794974ba4e95fb30d7964334894a98ae3dbec72ba7378",
      "seqno": 3318552
    },
    "revoke": {
      "sig_ids": [
        "f76151250a7db51f979cea604413006dfaf6997d093514b71cce6ed151c7f2cf0f"
      ]
    },
    "service": {
      "hostname": "sanday.gitlab.io",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1532390432,
  "expire_in": 157680000,
  "prev": "479a8d79eae7f115d4a79d38774ac940cae8a65e2ff2a02451269a393afa8b0c",
  "seqno": 39,
  "tag": "signature"
}

which yields the signature:

-----BEGIN PGP MESSAGE-----

owFtk3tsFEUcx68tpZRCkfAIqFhYMSmlwMzs7sxOWyABK/KqSgkklnrO7M4ea693
5e5oe5Sj2iC0miJCrIioKBRShYJF5FVKAYFAibyCUkqUIs9SJUR8QCI4VyWRxP1n
d2a/v998f5/fb5b3jnP1iNmmp6WEEDgc03KDu/JmedPKFO63wkpGmVIgul7Ca4lg
yF3gWEqGAiACGHGsWQAZUBgIYRMgW4NYmFRjHCEITBVgaumUWVxFwDQFtZjOMbMZ
BISpAjAlXbEdn0cEigKOLyTTahxyaFBkUEulgjABbF1nlm4RwCm2icUotzRhycC5
/mA0QprjLChGO365JxfuLnv/o3/oG0ATA9uyOQUccQFUXYUGhhhjoqk60SAkGkHy
C2FITKaqtkmIrQtmcpVhHVPS5Xt+VzqEOEWSA7GRYaoQc4ggg7Yqi7cFhTQqDIqA
jxUKqQ4yn8XCbiWSrhSKQIFXuAN+fyjK1gw5UQXUVaRSoFJN1seCc92FIsRkoG0K
IHQTCmEjbBGsCZ1QjRKNM01Q3eYqsAjFmqpqhqRPDSZUiwuTIM6ISgxpIyjm+fxK
hiqL1XUkHQREsb9ARA8POh5JLahk5Ck2wVBvXgCRDhixuA5tSqgpGAaaBlUAsGUz
G1NKLEBVHWqcQNlXLCyoQ1NCMG1gK/mR6HGBYsfsSh9t1CMARnuckJfxf1pWFPCH
/KbfK//ODYWKghlROqFwUVReIrj730Ru7vgsOSoyolgEgo7fJ2lJ5aPgNBWlK6K0
yAkItxNV6AQbQD7Rc0RxdMAIZYZkJZggNoS6pTEiZ80gRGNycIHJhCG7LJBtIwaQ
pkOEKVOpKqfW4MD8D0kqfTJPtCrH42Oh+QGhRHpWxardXDE9XEMGDut2wrkXt5qv
mJR3fVDNw+sVHxu9W66eiY893JnwoM+DZ0/3TvGXTr67YUvhmkBGVt9lFSMblMV3
109tunDpr3nvnNyQdej9yIqC6U+Ul4VdcZt7lVW5an7c9E1D2o6ZMfvDR2Zd3NJ9
bP9tX5oxx51xw5fMdlVmzzifOW6YYVtJdN26q/sObQPrUyc1Hb23rHtr8fLcVW0r
Gq78ltKtf1GjczycnZk4dk5ybk2vhDuH4qf/MrXf2PJ2NsXVL+HU3hOnj6ze03vE
seR7ofwfjr50+UGfYBm73Lbw2+rE3Nv+J1+49Ou5YNWyp3e27e6wSjdXfrhoQN8J
2E5TvzA9+xIqhp/fmLJzaLjlla3JSZ8mJXZv3g+u2uvndD4TaT9bV3LsFH5x1J1+
7dlbp6RMPJxf+tUnrUWj81rG5yx9bcBPr3tiaqtX7x5cHzqwdfbHdfXjKq+XHzOb
+n+/l2eNmmGOmYzOfXBroZV16mLJ0IQz8Uv/XDhpxNCrmanXws9VRvbcHGbvWuMB
j3e21+fc/86V2+ZOjR3c/PWtS8nzjpfU9ahobqlY1fF5w5WZG8e/9+ai4d7G5LO1
F/Mjd7f/Xny+Zl/Wux2bP8r8Y6L3ykq94kYtfHXTNHPQSft+x8iEG+mNm8rQgakT
kswjmZ1vp+O1jbVoafy1HXGNNSuvXT8zcMjR21kJHQM6c5zytfVzXq7bm11Wu6B1
2vPVF3LeyF+3YUzrz9XOwROLDzbFbt91+6m3dp5dkjrt5mfevwE=
=THcy
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/sanday_

==================================================================

